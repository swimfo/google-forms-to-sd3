"""
==============================================
convert_to_sd3.py
Google forms to .SD3 file converter
Christoff Kok Ceolecanths February 2022
Adapted from Liz Davidson NZ Masters Swimming March 2021's code/

Input to this module is a CSV file in the following format:

RegNo	    Registration number of the swimmer.
            No specific format is required, should be unique per swimmer in the file.
            Preferably the registration number as found in Team manager, else it won't match with that swimmer.
LastName	Swimmer last name
FirstName	Swimmer first name
DOB	        Swimmer date of birth in the format YYYY-MM-DD
Gender	    M or F
EventDistance	Numeric distance of the event, ie 50, 100, 200, 400, 800, 1500
EventStroke	Event stroke name - Freestyle, Backstroke, Breaststroke, Butterfly, IM (or Individual)
SeedTime	Event entry time in the format MM:SS.ff or NT (right padded) if no time has been provided
ClubCode	Club code, eg TSH, AUQ, etc or UNAT for Unattached, or another club code that can be invented (max 4 chars)
ClubName	Club name. E.g. Coelecanths

File should contain one row per swimmer per event that they have entered.
So if a swimmer has entered 6 events, there should be 6 rows for that swimmer.  
The swimmer and club details should be repeated on each row.
Order of rows in the file is not important, they will be sorted by Club/Swimmer

Parameters such as file names and meet details are contained in the config file:
config.yml
Edit the config file as required

==============================================
"""

import re
from datetime import datetime, date
import pandas as pd
import yaml

STROKE_INDEX_MAP = {
    'Freestyle': 1,
    'Backstroke': 2,
    'Breaststroke': 3,
    'Butterfly': 4,
    'Individual': 5,
    'IM': 5
}


# ==============================================
# Helper function to clean randomly entered time text into the format required by SDIF
# Time string must be of the form NN:NN.NN or else NT
# ==============================================
def format_entry_time(time_value: str, event_distance: int):
    # If it's 0 or 00 change to 'NT'
    if time_value == '0' or time_value == '00':
        time_value = 'NT'
    time_list = re.findall(r'\d+', time_value)
    # print(time_list)
    time_len = len(time_list)
    # If we only have 1 number then add 0 to the end
    if time_len == 1:
        time_list.append('00')
        time_len = 2
    # If we have 2 numbers, make an educated guess as to whether the first is minutes or seconds
    # based on its value and event distance
    if time_len == 2:
        # If it's less than 23 then it must be minutes - fastest 50m free record is just under 23s
        # so append 0 to the end
        if int(time_list[0]) < 23:
            time_list.append('00')
        # If it's between 23 and 59 and the distance is less than 200m assume it's seconds
        # so insert 0 minutes at the start
        elif int(time_list[0]) < 59 and event_distance < 200:
            time_list.insert(0, '00')
        # Otherwise assume it's minutes so append 0 
        else:
            time_list.append('00')
    time_len = len(time_list)
    # print(TimeList)
    # We should only have 3 elements by now
    if time_len == 3:
        time_value = str(time_list[0]).zfill(2) + ":" + str(time_list[1]).zfill(2) + "." + str(time_list[2]).zfill(2)
    else:
        # Yes - the spaces are necessary.
        # NT needs to be left justified, while if we have a time it is right justified. Who makes this stuff up?
        time_value = "NT      "
    return time_value


# ==============================================
# SDIF record type functions
# There are other record types to the ones below in the SDIF spec but these are the only ones 
# we actually need to import event entries into Meet Manager.
# ==============================================

def create_a0():
    """
    Record A0 - File description record
    :return: the record line
    """
    return "{0:2}{1:1}{2:8}{3:2}{4:30}{5:20}{6:10}{7:20}{8:12}{9:8}{10:42}{11:2}{12:3}\r".format(
        "A0", "2", "3.0", "01", "", "Google forms SDIF", "v0.01", "TSH Masters", "0725998176",
        date.today().strftime("%m%d%Y"), "", "", "")


# Record B1 - Meet record
def create_b1(meet_name: str, meet_start_date: date, meet_end_date: date, course_code: str):
    """
    Record B1 - Meet record
    :param meet_name: Name of the meet
    :param meet_start_date: Start date of the meet
    :param meet_end_date: End date of the meet
    :param course_code: Long / short meters / yards
    :return: the record line
    """
    # Emit B1 - Meet record
    return "{0:2}{1:1}{2:8}{3:30}{4:22}{5:22}{6:20}{7:2}{8:10}{9:3}{10:1}{11:8}{12:8}{13:4}{14:8}{15:1}{16:10}\r".format(
        "B1", "2", "", meet_name[:30], "", "", "", "", "", "NZL", "B", meet_start_date.strftime("%m%d%Y"),
        meet_end_date.strftime("%m%d%Y"),
        "", "", course_code, "")


def create_c1(club_code: str, club_name: str):
    """
    Record C1 - Team id record
    :param club_code: Club code
    :param club_name: Club name
    :return: the record line
    """
    return "{0:2}{1:1}{2:8}{3:6}{4:30}{5:16}{6:22}{7:22}{8:20}{9:2}{10:10}{11:3}{12:1}{13:6}{14:1}{15:10}\r".format(
        "C1", "2", "", "  " + club_code, club_name, "", "", "", "", "", "", "", "", "", "", "")


"""
# Don't think we really need a C2 record
def create_c2 (clubCode, clubName):
    return "{0:2}{1:1}{2:8}{3:6}{4:30}{5:12}{6:6}{7:6}{8:5}{9:6}{10:6}{11:16}{12:45}{13:1}{14:10}\r".format(
    "C2","2","", "  " + clubCode, "", "", "", "", "", "", "", "", "", "", "")
"""


# Record D0 - Individual event record
def create_D0(lastName, firstName, regNo, dateOfBirth, gender, eventDistance, eventStrokeCode, seedTime, courseCode,
              meetStartDate):
    fullName = lastName + ", " + firstName
    return "{0:2}{1:1}{2:8}{3:28}{4:12}{5:1}{6:3}{7:8}{8:2}{9:1}{10:1}{11:>4}{12:1}{13:>4}{14:4}{15:8}{16:>8}{17:1}{18:63}\r".format(
        "D0", "2", "", fullName[:28], regNo, "", "", dateOfBirth.strftime("%m%d%Y"), "", gender, "X", eventDistance,
        eventStrokeCode, "", "20OV", meetStartDate.strftime("%m%d%Y"), seedTime, courseCode, ""
    )


def create_d1(full_name: str, reg_no, date_of_birth: date, gender: str, club_code):
    """
    Record D1 - Individual administrative record
    :param full_name: First names + surname, seperated by a comma and space
    :param reg_no: registration number
    :param date_of_birth: the individual's date of birth
    :param gender: the individual's gender
    :param club_code:
    :return: the record line
    """
    return "{0:2}{1:1}{2:8}{3:6}{4:1}{5:28}{6:1}{7:12}{8:1}{9:3}{10:8}{11:2}{12:1}{13:86}\r".format(
        "D1", "2", "", "  " + club_code, "", full_name[:28], "", reg_no, "", "", date_of_birth.strftime("%m%d%Y"), "",
        gender, ""
    )


def create_z0():
    """
    Record Z0 - File terminator record
    :return: the record line
    """
    #
    return "{0:2}{1:1}{2:8}{3:2}{4:147}\r".format("Z0", "2", "", "01", "")


# ==============================================
# Process the CSV file to SDIF
# ==============================================
def SDIFCreate():
    cfg = yaml.safe_load(open('config.yml'))

    # Read parameters from config file
    # Note that dates are read into a datetime object rather than a string
    sdif_cs_vfile = cfg['filenames']['InFile']
    sdif_out_file = cfg['filenames']['OutFile']
    course_code = cfg['meetinfo']['CourseCode']
    meet_name = cfg['meetinfo']['MeetName']
    meet_start_date = cfg['meetinfo']['MeetStartDate']
    meet_end_date = cfg['meetinfo']['MeetEndDate']

    # Process with data frames
    df_entries = pd.read_csv(sdif_cs_vfile)
    df_entries.sort_values(['ClubCode', 'LastName', 'FirstName'], inplace=True)

    # Add A0 and B1 line at the start of the file
    sd3_lines = [
        create_a0(),
        create_b1(meet_name, meet_start_date, meet_end_date, course_code)
    ]
    clubCodeLast = 'x'
    fullNameLast = 'x'
    last_d1_record = None
    for i, row in df_entries.iterrows():
        reg_no = row['RegNo']
        last_name = row['LastName']
        first_name = row['FirstName']
        date_of_birth = datetime.strptime(row['DOB'], '%Y-%m-%d')
        gender = row['Gender']
        event_distance = row['EventDistance']
        event_stroke = row['EventStroke']
        event_stroke_code = STROKE_INDEX_MAP[event_stroke]
        seed_time = row['SeedTime']
        club_code = row['ClubCode']
        club_name = row['ClubName']

        full_name = last_name + ", " + first_name

        # Output a C1 line per club
        if club_code != clubCodeLast:
            if fullNameLast != 'x':
                # Output a D1 line after all D0 lines for a swimmer on change of club
                sd3_lines.append(last_d1_record)
                fullNameLast = 'x'
            sd3_lines.append(create_c1(club_code, club_name))
            # sd3lines.append(EmitC2(clubCode, clubName))
            clubCodeLast = club_code

        if full_name != fullNameLast and fullNameLast != 'x':
            # Output a D1 line after all D0 lines for a swimmer
            sd3_lines.append(last_d1_record)

        fullNameLast = full_name

        # Output a D0 line per entry
        sd3_lines.append(
            create_D0(
                last_name, first_name, reg_no, date_of_birth, gender,
                event_distance, event_stroke_code, seed_time, course_code, meet_start_date
            )
        )

        last_d1_record = create_d1(full_name, reg_no, date_of_birth, gender, club_code)

        # print(regNo, lastName, firstName, dateOfBirth, gender, eventDistance, eventStroke, eventStrokeCode, seedTime)

    if last_d1_record:
        sd3_lines.append(last_d1_record)

    # Output a Z0 line at the end of the file
    sd3_lines.append(create_z0())

    with open(sdif_out_file, 'w') as f:
        f.writelines(sd3_lines)
